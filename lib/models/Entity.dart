import 'package:cloud_firestore/cloud_firestore.dart';

class EntityRecord {
  final String name;
  final String votes;
  final DocumentReference reference;

  EntityRecord.fromMap(Map<String, dynamic> map, {this.reference})
      : assert(map['name'] != null),
        assert(map['id'] != null),
        name = map['name'],
        votes = map['id'];


  EntityRecord.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data, reference: snapshot.reference);

  @override
  String toString() => "Record<$name:$votes>";
}