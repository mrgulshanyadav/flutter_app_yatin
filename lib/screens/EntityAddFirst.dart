import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_yatin/screens/HomeScreen.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EntityAddFirst extends StatefulWidget {
  @override
  _EntityAddFirstState createState() => _EntityAddFirstState();
}

class _EntityAddFirstState extends State<EntityAddFirst> {
  String name,id,image_url,address1,address2,state,country,postalcode,viewEnable,entitytype,gender;

  List<String> countryList;
  List<String> StatesList;


  getCurrentUserDetails() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    setState(() {
      name =  user.displayName;
      id = user.uid;
    });
  }

  @override
  void initState() {
    name = "";
    id = "";
    getCurrentUserDetails();
    image_url = "";
    address1 = "";
    address2 = "";
    state = "Delhi";
    country = "India";
    postalcode = "";
    viewEnable = "true";
    entitytype = "Personal";
    gender = "Male";

    countryList = new List();
    StatesList = new List();
    countryList = ['India', 'USA', 'France', 'China'];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(title: Text("Add Entity"),),
        body: SafeArea(
          child: Container(
            margin: EdgeInsets.all(10),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(15),
                        filled: true,
                        fillColor: Color(0xFFF3F7F6),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black12),
                        ),
                        hintText: 'Name',
                        prefixIcon: Icon(Icons.person),
                      ),
                      onChanged: (input){
                        setState((){
                          name = input;
                        });
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(15),
                        filled: true,
                        fillColor: Color(0xFFF3F7F6),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black12),
                        ),
                        hintText: 'Image_url',
                        prefixIcon: Icon(Icons.image),
                      ),
                      onChanged: (input){
                        setState((){
                          image_url = input;
                        });
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(15),
                        filled: true,
                        fillColor: Color(0xFFF3F7F6),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black12),
                        ),
                        hintText: 'address line 1',
                        prefixIcon: Icon(Icons.format_list_numbered),
                      ),
                      onChanged: (input){
                        setState((){
                          address1 = input;
                        });
                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(15),
                        filled: true,
                        fillColor: Color(0xFFF3F7F6),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black12),
                        ),
                        hintText: 'address line 2',
                        prefixIcon: Icon(Icons.format_list_numbered),
                      ),
                      onChanged: (input){
                        setState((){
                          address2 = input;
                        });
                      },
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom: 10,left: 10),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: state,
                            hint: Text("State"),
                            isDense: true,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            onChanged: (String newValue) {
                              setState(() {
                                state = newValue;
                              });
                            },
                            items: StatesList.map<DropdownMenuItem<String>>((String val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val),
                              );
                            }).toList(),

                          )
                      )
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom: 10,left: 10),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: country,
                            hint: Text("Country"),
                            isDense: true,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            onChanged: (String newValue) {
                              setState(() {
                                country  = newValue;
                              });
                              if(newValue=='India'){
                                setState(() {
                                  StatesList.clear();
                                  StatesList = ['Delhi', 'Uttar Pradesh', 'Haryana', 'Himachal Pradesh'];
                                  state = 'Delhi';
                                });
                              }
                            },
                            items: countryList.map<DropdownMenuItem<String>>((String val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val),
                              );
                            }).toList(),
                          )
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10),
                    child: TextField(
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.all(15),
                        filled: true,
                        fillColor: Color(0xFFF3F7F6),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15.0)),
                          borderSide: BorderSide(color: Colors.black12),
                        ),
                        hintText: 'Postal Code',
                        prefixIcon: Icon(Icons.pin_drop),
                      ),
                      onChanged: (input){
                        setState((){
                          postalcode = input;
                        });
                      },
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom: 10,left: 10),
                      child: DropdownButtonHideUnderline(
                          child: DropdownButton<String>(
                            value: entitytype,
                            icon: Icon(Icons.keyboard_arrow_down),
                            iconSize: 24,
                            onChanged: (String newValue) {
                              setState(() {
                                entitytype = newValue;
                              });
                            },
                            items: <String>['Personal', 'Comercial'].map<DropdownMenuItem<String>>((String val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val),
                              );
                            })
                                .toList(),
                          )
                      )
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 10,left: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Gender",style: TextStyle(fontSize: 16),),
                        RadioButtonGroup(
                          orientation: GroupedButtonsOrientation.HORIZONTAL,
                          margin: const EdgeInsets.only(left: 0.0),
                          onSelected: (String selected) => setState((){
                            gender = selected;
                          }),
                          labels: <String>[
                            "Male",
                            "Female",
                            "Other"
                          ],
                          picked: gender,
                          itemBuilder: (Radio rb, Text txt, int i){
                            return Column(
                              children: <Widget>[
                                rb,
                                txt,
                              ],
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: ButtonTheme(
                      minWidth: double.infinity,
                      child: FlatButton(
                        onPressed: () async {

                          if(name.isEmpty){
                            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Enter Name!")));
                          }
                          else if(image_url.isEmpty){
                            Scaffold.of(context).showSnackBar(SnackBar(content: Text("Enter Image Url!")));
                          }
                          else{

                            FirebaseUser user = await FirebaseAuth.instance.currentUser();

                            Map<String,dynamic> entityMap = Map();
                            entityMap.putIfAbsent("name", ()=> name);
                            entityMap.putIfAbsent("id", ()=> user.uid);
                            entityMap.putIfAbsent("image_url", ()=> image_url);
                            entityMap.putIfAbsent("address1", ()=> address1);
                            entityMap.putIfAbsent("address2", ()=> address2);
                            entityMap.putIfAbsent("state", ()=> state);
                            entityMap.putIfAbsent("country", ()=> country);
                            entityMap.putIfAbsent("postalcode", ()=> postalcode);
                            entityMap.putIfAbsent("entitytype", ()=> entitytype);
                            entityMap.putIfAbsent("gender", ()=> gender);

                            Firestore.instance.collection("Entity").document(user.uid).setData(entityMap).then((onValue){
                              setState(() {
                                name = "";
                                image_url = "";
                                address1 = "";
                                address2 = "";
                                state = "Delhi";
                                country = "India";
                                postalcode = "";
                                viewEnable = "true";
                                entitytype = "Personal";
                                gender = "Male";
                              });
                              
                              Navigator.of(context).push(MaterialPageRoute(builder: (context)=> HomeScreen()));
                            }).catchError((error){
                              Scaffold.of(context).showSnackBar(SnackBar(content: Text(error)));
                            });

                          }

                        },
                        child: Text("Add Entity",style: TextStyle(color: Colors.white),),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5)
                        ),
                        color: Colors.redAccent,
                        padding: EdgeInsets.all(15),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ),
    );
  }

}


