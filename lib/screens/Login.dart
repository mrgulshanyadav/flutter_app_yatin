import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_app_yatin/screens/EntityAddFirst.dart';
import 'package:flutter_app_yatin/screens/HomeScreen.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

class _LoginState extends State<Login> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            FlatButton(
              color: Colors.white,
              child: Text("Sign in with google", style: TextStyle(color: Colors.grey),),
              onPressed: () {
                signInWithGoogle().then((FirebaseUser user) async {

                  Firestore.instance.collection("Entity").document(user.uid).get().then((datasnapshot){
                    if(datasnapshot.exists){
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context){
                            return HomeScreen();
                          }
                      ));
                    }
                    else{
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context){
                            return EntityAddFirst();
                          }
                      ));
                    }

                  }).catchError((onError){
                    print(onError);
                  });

                });
                },
            )
          ],
        ),
      ),
    );
  }

  Future<FirebaseUser> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return currentUser;

  }

  void signOutGoogle() async{
    await googleSignIn.signOut();

    print("User Sign Out");
  }

}

