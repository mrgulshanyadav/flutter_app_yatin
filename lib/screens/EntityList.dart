import 'package:flutter/material.dart';
import 'package:flutter_app_yatin/models/Entity.dart';
import 'package:flutter_app_yatin/widgets/EntityTile.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../repository/FirestoreEntityFetch.dart';

class EntityList extends StatefulWidget {

  @override
  _EntityListState createState() {
    return _EntityListState();
  }
}

class _EntityListState extends State<EntityList> {
  List<EntityRecord> _entityRecordList = <EntityRecord>[];

  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('Entity').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();
        snapshot.data.documents.forEach((data) {
          _entityRecordList.add(EntityRecord.fromSnapshot(data));
        }
        );
        return _buildList(context);
      },
    );
  }

  Widget _buildList(BuildContext context) =>
      Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(
                      labelText: "Search It",
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(25.0)))),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _entityRecordList.length,
                  itemBuilder: (context, index) =>
                      EntityTile(_entityRecordList[index]),
                ),
              ),
            ],
          ),
        ),
      );

}