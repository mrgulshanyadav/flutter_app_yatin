import 'package:flutter/material.dart';
import '../repository/beer_repository.dart';
import '../models/beer.dart' show Beer;
import '../widgets/beer_tile.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<Beer> _beers = <Beer>[];
  TextEditingController controller = new TextEditingController();
  String filter;

  @override
  void initState() {
    super.initState();
    listenForBeers();
  }

  @override
  Widget build(BuildContext context) =>
      Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Top Start'),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  onChanged: (value) {
                    filterSearchResults(value);
                  },
                  controller: controller,
                  decoration: InputDecoration(
                      labelText: "Search It",
                      hintText: "Search",
                      prefixIcon: Icon(Icons.search),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                              Radius.circular(25.0)))),
                ),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: _beers.length,
                  itemBuilder: (context, index) => BeerTile(_beers[index]),
                ),
              ),
            ],
          ),
        ),
      );

  void listenForBeers() async {
    final Stream<Beer> stream = await getBeers();
    stream.listen((Beer beer) =>
        setState(() => _beers.add(beer))
    );
  }


  void filterSearchResults(String query) {
    List<Beer> dummySearchList = List<Beer>();
    dummySearchList.addAll(_beers);
    if (query.isNotEmpty) {
      List<Beer> dummyListData = List<Beer>();
      dummySearchList.forEach((item) {
        if (item.name.contains(query)) {
          dummyListData.add(item);
        }
      });
      setState(() {
        _beers.clear();
        _beers.addAll(dummyListData);
      });
      return;
    } else {
      setState(() {
        _beers.clear();
        _beers.addAll(_beers);

      });
    }
  }
//  Widget _buildBody(BuildContext context) {
//    return StreamBuilder<QuerySnapshot>(
//      stream: Firestore.instance.collection('1001').snapshots(),
//      builder: (context, snapshot) {
//        if (!snapshot.hasData) return LinearProgressIndicator();
//
//        //return _buildList(context, snapshot.data.documents);
//        //snapshot.map((data) => _buildListItem(context, data)).toList()
//        //final record = Record.fromSnapshot(data);
//
//        //return snapshot.data.documents.map((data) => Beer.fromJSON(data))
//      },
//    );
//  }


}