import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../main.dart';
import 'EntityAdd.dart';
import 'EntityList.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        initialIndex: 1,
        child: new Scaffold(
          appBar: new AppBar(
            title: new Text("Home Page"),
            bottom: new TabBar(tabs: <Tab>[
              new Tab( icon: new Icon(Icons.person)),
              new Tab( icon: new Icon(Icons.home)),
              new Tab( icon: new Icon(Icons.search)),
              new Tab( icon: new Icon(Icons.add_box)),
            ]),
          ),
          body: TabBarView(children: <Widget>[
            new FlatButton(
              child: Text("Logout"),
              onPressed: (){
                FirebaseAuth.instance.signOut();
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=> MyApp()));
              },
            ),
            new EntityList(),
            new Text("3rd Tab"),
            new EntityAdd(),
          ]),
        )
    );
  }
}
