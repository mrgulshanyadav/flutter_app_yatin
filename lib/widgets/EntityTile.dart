import 'package:flutter/material.dart';
import 'package:flutter_app_yatin/models/Entity.dart';


class EntityTile extends StatelessWidget {
  final EntityRecord _record;
  EntityTile(this._record);


  @override
  Widget build(BuildContext context) => Column(
    children: <Widget>[
      ListTile(
        title: Text(_record.name),
        subtitle: Text(_record.votes),
        leading: Container(
            margin: EdgeInsets.only(left: 6.0),
            child: Image.network(_record.name, height: 50.0, fit: BoxFit.fill,)
        ),
      ),
      Divider()
    ],
  );
}