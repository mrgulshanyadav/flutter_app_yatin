import 'package:flutter/material.dart';
import 'package:flutter_app_yatin/screens/EntityList.dart';
import 'package:flutter_app_yatin/screens/HomeScreen.dart';
import 'package:flutter_app_yatin/screens/Login.dart';
import 'screens/EntityAdd.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Home Page',
        routes: <String, WidgetBuilder>{
          '/EntityList': (BuildContext context) => new EntityList(),
        },
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: SafeArea(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Login()
                  ],
                )
              )
          ),
        )
    );
  }
}